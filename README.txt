ASP .NET 5 (MVC 6) HATEOAS CRUD Operations Library
				"aspnet.hop"

I. Requirements:
	1. ASP .NET 5 (MVC 6) project with MVC enabled. ("app.UseMvc();")
	2. Entity Framework 7 with any data source, with one or many contexts.
	3. LINQ

II. Usage:
	1. Add the aspnet.hop dependency into your API's project.json. Example:
		"dependencies": {
			"Microsoft.ApplicationInsights.AspNet": "1.0.0-rc1",
			"Microsoft.AspNet.IISPlatformHandler": "1.0.0-rc1-final",
			"Microsoft.AspNet.Mvc": "6.0.0-rc1-final",
			......................................
			"aspnet.hop": "1.0.0"
		}
	2. In the Startup class, in the Configure(...) method, call app.UseHateoas(...) AFTER app.UseMvc();. Example:
		app.UseMvc();
		..........................................
		app.UseHateoas(options => {
			options.SetBaseUrl("hateoas")
            .AddEndpoint(new HateoasEndpoint<ExampleContext, Person>("people"))
            .AddEndpoint(new HateoasEndpoint<ExampleContext, House>("houses"))
            .AddEndpoint(new HateoasEndpoint<ExampleContext, Area>("areas"));
        });
		
III. Library configurations
	app.UseHateoas(options => {
		options.SetBaseUrl(string); (1) Will modify the beggining of the URLs
		options.AddEndpoint(new HateoasEndpoint<ContextType, EntityType>("the_url")); (2) Will register a new endpoint
	});


IV. How it works:
	Using the app.UseHateoas(options) method, all our endpoint definitions and settings will be saved.
	For each endpoint, some CRUD routes will be registered into MVC, pointing to the non-generic dummy controller 
AbstractHateoasController.cs, which implements all the methods found in HateoasController, only they throw a NotImplementedException.
	All routes for the specific endpoint will be given a custom handler (like MvcRouteHandler.cs).
	The custom handler will override the controller definition, and will modify the controller type from AbstractHateoasController
to HateoasController<TContext, TEntity>. This handler is much like a normal one, only it overrides the controller definitions.
	The Controller Factory behind the handler will not care about what we did and will route the URLs
perfectly to a HateoasController<TContext, TEntity> instance. PS: DI still works, this
"forcefully"-instantiated controller behaves absolutely normal.
	In HateoasController we have the implementation for the HATEOAS CRUD operations.