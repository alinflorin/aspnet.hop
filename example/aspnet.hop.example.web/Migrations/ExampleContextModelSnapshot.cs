using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using aspnet.hop.example.web.Models.DB;

namespace aspnet.hop.example.web.Migrations
{
    [DbContext(typeof(ExampleContext))]
    partial class ExampleContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("aspnet.hop.example.web.Models.DB.Area", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 3);

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");
                });

            modelBuilder.Entity("aspnet.hop.example.web.Models.DB.House", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AreaId");

                    b.Property<string>("Description");

                    b.Property<int>("Number");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("aspnet.hop.example.web.Models.DB.Person", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email");

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<int>("HouseId");

                    b.Property<string>("LastName")
                        .IsRequired();

                    b.Property<string>("Phone");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("aspnet.hop.example.web.Models.DB.House", b =>
                {
                    b.HasOne("aspnet.hop.example.web.Models.DB.Area")
                        .WithMany()
                        .HasForeignKey("AreaId");
                });

            modelBuilder.Entity("aspnet.hop.example.web.Models.DB.Person", b =>
                {
                    b.HasOne("aspnet.hop.example.web.Models.DB.House")
                        .WithMany()
                        .HasForeignKey("HouseId");
                });
        }
    }
}
