﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace aspnet.hop.example.web.Models.DB
{
    public static class Seeder
    {
        public static void Seed(ExampleContext db)
        {
            if (db.Areas.Any())
            {
                return;
            }

            var p1 = new Person
            {
                Email = "john.doe@isp.com",
                FirstName = "John",
                LastName = "Doe",
                Phone = "1234567890"
            };

            var p2 = new Person
            {
                Email = "jane.doe@isp.com",
                FirstName = "Jane",
                LastName = "Doe"
            };

            var h1 = new House
            {
                People = new List<Person>() { p1, p2 },
                Description = "Nice house",
                Number = 333
            };


            var p3 = new Person
            {
                Email = "gigi.ion@isp.com",
                FirstName = "Gigi",
                LastName = "Ion"
            };

            var h2 = new House
            {
                People = new List<Person>() { p3 },
                Description = "Ugly house",
                Number = 336
            };

            var area1 = new Area
            {
                Code = "CPK",
                Name = "Central Park",
                Houses = new List<House>() { h1, h2 }
            };

            db.Areas.Add(area1);

            var p4 = new Person
            {
                Email = "radu.alexandru@isp.com",
                FirstName = "Radu",
                LastName = "Alexandru"
            };

            var h3 = new House
            {
                People = new List<Person>() { p4 },
                Number = 337,
            };

            var area2 = new Area
            {
                Code = "NY",
                Houses = new List<House>() { h3 },
                Name = "New York"
            };

            db.Areas.Add(area2);


            db.SaveChanges();
        }
    }
}
