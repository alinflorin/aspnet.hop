﻿using Microsoft.Data.Entity;

namespace aspnet.hop.example.web.Models.DB
{
    public class ExampleContext : DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<House> Houses { get; set; }
        public DbSet<Area> Areas { get; set; }
    }
}
