﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace aspnet.hop.example.web.Models.DB
{
    public class House
    {
        public int Id { get; set; }
        [Required]
        public int AreaId { get; set; }
        public Area Area { get; set; }
        [Required]
        public int Number { get; set; }
        public string Description { get; set; }

        public List<Person> People { get; set; }
    }
}
