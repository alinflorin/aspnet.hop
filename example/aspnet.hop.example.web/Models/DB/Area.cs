﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace aspnet.hop.example.web.Models.DB
{
    public class Area
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(3)]
        [MinLength(2)]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }

        public List<House> Houses { get; set; }
    }
}
