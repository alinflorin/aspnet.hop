﻿using System.ComponentModel.DataAnnotations;

namespace aspnet.hop.example.web.Models.DB
{
    public class Person
    {
        public int Id { get; set; }
        [Required]
        public int HouseId { get; set; }
        public House House { get; set; }
        [Required]
        [MinLength(3)]
        public string FirstName { get; set; }
        [Required]
        [MinLength(3)]
        public string LastName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
