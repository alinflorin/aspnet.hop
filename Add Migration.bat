@echo off
cd example\aspnet.hop.example.web
set /p ctx="Name of context (empty for all): "
set /p mname="Name of the migration: "
IF DEFINED ctx GOTO xxx
FOR /F "tokens=*" %%F in ('dnx ef dbcontext list') DO ( 
    dnx ef migrations add %mname% --context %%F
)
:xxx
IF DEFINED ctx dnx ef migrations add %mname% --context %ctx%
pause