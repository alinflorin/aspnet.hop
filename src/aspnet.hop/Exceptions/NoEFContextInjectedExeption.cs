﻿using System;
namespace aspnet.hop.Exceptions
{
    /// <summary>
    /// Exception thrown when no EF context was found
    /// </summary>
    public sealed class NoEfContextInjectedExeption : Exception
    {
        public NoEfContextInjectedExeption(string message):base(message)
        {

        }
    }
}
