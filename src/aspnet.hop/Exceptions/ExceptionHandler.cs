﻿using Microsoft.AspNet.Mvc;
using System;

namespace aspnet.hop.Exceptions
{
    /// <summary>
    /// Used for nicer error and exception handling in HateoasController
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public sealed class ExceptionHandler<TEntity>
    {
        public IActionResult Error(string message)
        {
            return new BadRequestObjectResult("HATEOAS Library error @ endpoint " + typeof(TEntity).Name + " :" + message);
        }

        public IActionResult NotFound(string message)
        {
            return new HttpNotFoundObjectResult("HATEOAS Library Not Found @ endpoint " + typeof(TEntity).Name + " :" + message);
        }

        public IActionResult Exception(Exception e)
        {
            return new BadRequestObjectResult("HATEOAS Library exception @ endpoint " + typeof(TEntity).Name + " :" + e.GetType().Name + " - " + e.Message);
        }
    }
}
