﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("aspnet.hop")]
[assembly: AssemblyDescription("ASP .NET 5 (MVC 6) HATEOAS CRUD API Library for CLR and CORECLR.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Alin")]
[assembly: AssemblyProduct("aspnet.hop")]
[assembly: AssemblyCopyright("Copyright ©  2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d8beadb6-49e7-4cef-8870-19d02197b28b")]
