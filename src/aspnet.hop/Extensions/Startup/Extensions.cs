﻿using aspnet.hop.Metadata;
using aspnet.hop.Routing;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Routing;
using Microsoft.Data.Entity;
using System;

namespace aspnet.hop.Extensions.Startup
{
    /// <summary>
    /// Point of entry in the library - this is called in Startup.cs
    /// </summary>
    public static class Extensions
    {
        // This makes sure HATEOAS is initialised only once
        private static bool _isHateoasInitialised;

        /// <summary>
        /// This is called in Startup's Configure method
        /// </summary>
        /// <param name="app">Auto-injected into extension</param>
        /// <param name="options">The object populated by lambda</param>
        public static void UseHateoas(this IApplicationBuilder app, Action<HateoasOptions> options)
        {
            if (_isHateoasInitialised)
            {
                return;
            }
            var opts = new HateoasOptions(app);
            options.Invoke(opts);
            var col = new RouteCollection();
            //Register the main route - lists all endpoints URLs
            var rb = new RouteBuilder
            {
                DefaultHandler = new HateoasEndpointListRouteHandler(opts), //Mapping to handler
                ServiceProvider = app.ApplicationServices
            };
            rb.MapRoute( //Read
                name: "hateoas_all",
                template: opts.GetBaseUrl(),
                defaults: new { controller = "AbstractHateoas", action = "EndpointsList" },
                constraints: new { httpMethod = new HttpMethodRouteConstraint("GET") }
            );
            col.Add(rb.Build());
            //Let's create routes for all endpoints
            foreach (var endpoint in opts.GetEndPoints())
            {
                col.Add(endpoint.BuildRoutes());
            }
            app.UseRouter(col); //Use the newly-created routes, too
            _isHateoasInitialised = true; //Finished
        }
    }
}
