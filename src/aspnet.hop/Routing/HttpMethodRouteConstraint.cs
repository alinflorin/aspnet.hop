﻿using Microsoft.AspNet.Routing;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;

namespace aspnet.hop.Routing
{
    /// <summary>
    /// A new constraint for routes, which checks their HTTP method on the request.
    /// </summary>
    public sealed class HttpMethodRouteConstraint : IRouteConstraint
    {
        private readonly List<string> _httpMethods = new List<string>();

        public HttpMethodRouteConstraint(string hm)
        {
            _httpMethods.Add(hm.ToLower());
        }

        public HttpMethodRouteConstraint(IEnumerable<string> httpMethods)
        {
            _httpMethods.AddRange(httpMethods.Select(m => m.ToLower()).ToList());
        }
        public bool Match(HttpContext httpContext, IRouter route, string routeKey, IDictionary<string, object> values, RouteDirection routeDirection)
        {
            return _httpMethods.Contains(httpContext.Request.Method.ToLower());
        }
    }
}
