﻿using aspnet.hop.Metadata;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Routing;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace aspnet.hop.Routing
{
    /// <summary>
    /// Custom route handler specially made for endpoint listing "http(s)://AppUrl/BaseHateoasUrl/"
    /// </summary>
    public sealed class HateoasEndpointListRouteHandler : IRouter
    {
        private HateoasOptions _opts;

        public HateoasEndpointListRouteHandler(HateoasOptions opts)
        {
            _opts = opts;
        }

        public VirtualPathData GetVirtualPath(VirtualPathContext context)
        {
            return null;
        }

        /// <summary>
        /// We write the JSON directly from the handler - no need for controller for such an easy task
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task RouteAsync(RouteContext context)
        {
            var result = new Dictionary<string, string>();
            foreach (var endpoint in _opts.GetEndPoints().OrderBy(e => e.GetEntityType().Name).ToList())
            {
                result[endpoint.GetUrl()] = GetApplicationUrlTrailingSlash(context.HttpContext) + _opts.GetBaseUrl() + "/" + endpoint.GetUrl();
            }
            await context.HttpContext.Response.WriteAsync(JsonConvert.SerializeObject(result));
        }

        /// <summary>
        /// Gets the webapp URL
        /// </summary>
        /// <param name="context">The HTTP Context</param>
        /// <returns>The app URL</returns>
        private string GetApplicationUrlTrailingSlash(HttpContext context)
        {
            var url = "http://";
            if (context.Request.IsHttps)
            {
                url = "https://";
            }
            url += context.Request.Host + "/";
            return url;
        }
    }
}
