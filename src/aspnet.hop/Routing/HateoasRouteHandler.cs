﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Routing;
using Microsoft.AspNet.Mvc.Infrastructure;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Mvc.Controllers;
using aspnet.hop.Controllers;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNet.Mvc.Abstractions;
using Microsoft.AspNet.Mvc;
using System.Reflection;

namespace aspnet.hop.Routing
{
    /// <summary>
    /// Custom ASP .NET MVC route handler - this fools the routes to go into our own HateoasController
    /// This is quite like MvcRouteHandler.cs from aspnet's repo
    /// </summary>
    /// <typeparam name="TContext">EF Context type</typeparam>
    /// <typeparam name="TEntity">Model type</typeparam>
    public sealed class HateoasRouteHandler<TContext, TEntity> : IRouter where TContext : DbContext where TEntity : class
    {
        // Standard routing services from ASPNET
        private bool _servicesRetrieved;
        private IActionContextAccessor _actionContextAccessor;
        private IActionInvokerFactory _actionInvokerFactory;
        private IActionSelector _actionSelector;

        /// <summary>
        /// Not used
        /// </summary>
        /// <param name="context"></param>
        /// <returns>null</returns>
        public VirtualPathData GetVirtualPath(VirtualPathContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            return null;
        }

        /// <summary>
        /// This does the actual routing - uses a controller factory and sends the request context to the controller
        /// We intervined...
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Nothing</returns>
        public async Task RouteAsync(RouteContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            EnsureServices(context.HttpContext);
            // Contains routing data
            var actionDescriptor = (await _actionSelector.SelectAsync(context)) as ControllerActionDescriptor;
            if (actionDescriptor == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            if (actionDescriptor.RouteValueDefaults != null)
            {
                foreach (var kvp in actionDescriptor.RouteValueDefaults)
                {
                    if (!context.RouteData.Values.ContainsKey(kvp.Key))
                    {
                        context.RouteData.Values.Add(kvp.Key, kvp.Value);
                    }
                }

                // Removing RouteGroup from RouteValues to simulate the result of conventional routing
                //context.RouteData.Values.Remove(TreeRouter.RouteGroupKey);
            }

            // Fool the router to go to HateoasController instead of the crippled AbstractHateoasController
            actionDescriptor.ControllerName = "Hateoas"; // Change the name
            actionDescriptor.ControllerTypeInfo = typeof(HateoasController<TContext, TEntity>).GetTypeInfo(); // Replace the type with the exact one WTF HACK
            actionDescriptor.DisplayName = typeof(HateoasController<TContext, TEntity>).FullName; // Here, too
            await InvokeActionAsync(context.HttpContext, actionDescriptor, context.RouteData); //Go!
            context.IsHandled = true;
        }

        private async Task InvokeActionAsync(HttpContext httpContext, ActionDescriptor actionDescriptor, RouteData rd)
        {
            var routeData = rd;
            try
            {
                var actionContext = new ActionContext(httpContext, routeData, actionDescriptor);
                if (_actionContextAccessor != null)
                {
                    _actionContextAccessor.ActionContext = actionContext;
                }
                var invoker = _actionInvokerFactory.CreateInvoker(actionContext);
                if (invoker == null)
                {
                    throw new InvalidOperationException();
                }
                await invoker.InvokeAsync(); // The heart of the library
            }
            catch (Exception e)
            {
                httpContext.Response.StatusCode = 500;
                await httpContext.Response.WriteAsync("HATEOAS Library error (" + e.GetType().Name + "): " + e.Message);
            }
        }

        /// <summary>
        /// Gets the services from ASPNET's DI container
        /// </summary>
        /// <param name="context">The HTTP context</param>
        private void EnsureServices(HttpContext context)
        {
            if (_servicesRetrieved)
            {
                return;
            }
            var services = context.RequestServices;
            MvcServicesHelper.ThrowIfMvcNotRegistered(services);
            _actionContextAccessor = services.GetService<IActionContextAccessor>();
            _actionInvokerFactory = services.GetRequiredService<IActionInvokerFactory>();
            _actionSelector = services.GetRequiredService<IActionSelector>();
            _servicesRetrieved = true;
        }
    }
}