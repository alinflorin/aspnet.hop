﻿using System;
using System.Linq;
using aspnet.hop.Metadata;

namespace aspnet.hop.Helpers
{
    /// <summary>
    /// Helper class for generating absolute URLs to HATEOAS CRUD actions
    /// </summary>
    public static class HateoasUrls
    {
        /// <summary>
        /// Will generate URL for single-entity operations which requires sending the ID - Show, Delete
        /// </summary>
        /// <typeparam name="TEntityType">The EF Entity's type</typeparam>
        /// <param name="operation">CRUD operation type from the HateoasOperations enum</param>
        /// <param name="id">The PK value</param>
        /// <returns>Absolute URL as string</returns>
        public static string Get<TEntityType>(HateoasOperations operation, object id)
        {
            if (operation == HateoasOperations.Create || operation == HateoasOperations.Update || operation == HateoasOperations.List || id == null)
            {
                throw new ArgumentException("The operation doesn't require an ID.");
            }
            var endpoint = HateoasOptions.Endpoints.FirstOrDefault(e => e.GetEntityType() == typeof(TEntityType));
            if (endpoint == null)
            {
                throw new ArgumentException("The model doesn't have a HATEOAS endpoint set up.");
            }
            var url = "";
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (operation)
            {
                    case HateoasOperations.Show:
                    url = $"/{HateoasOptions.BaseUrl}/{endpoint.GetUrl()}/{id}";
                    break;

                    case HateoasOperations.Delete:
                    url = $"/{HateoasOptions.BaseUrl}/{endpoint.GetUrl()}/{id}";
                    break;
            }
            if (string.IsNullOrEmpty(url))
            {
                throw new ArgumentException("The operation is invalid.");
            }
            return url;
        }

        /// <summary>
        /// Will generate URL for single-entity operations which don't require sending the ID - Create, List, Update
        /// </summary>
        /// <typeparam name="TEntityType">The EF Entity's type</typeparam>
        /// <param name="operation">CRUD operation type from the HateoasOperations enum</param>
        /// <returns>Absolute URL as string</returns>
        public static string Get<TEntityType>(HateoasOperations operation)
        {
            if (operation == HateoasOperations.Show || operation == HateoasOperations.Delete)
            {
                throw new ArgumentException("The operation required an ID.");
            }
            var endpoint = HateoasOptions.Endpoints.FirstOrDefault(e => e.GetEntityType() == typeof(TEntityType));
            if (endpoint == null)
            {
                throw new ArgumentException("The model doesn't have a HATEOAS endpoint set up.");
            }
            var url = "";
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (operation)
            {
                case HateoasOperations.Create:
                    url = $"/{HateoasOptions.BaseUrl}/{endpoint.GetUrl()}";
                    break;

                case HateoasOperations.List:
                    url = $"/{HateoasOptions.BaseUrl}/{endpoint.GetUrl()}";
                    break;

                case HateoasOperations.Update:
                    url = $"/{HateoasOptions.BaseUrl}/{endpoint.GetUrl()}";
                    break;
            }
            if (string.IsNullOrEmpty(url))
            {
                throw new ArgumentException("The operation is invalid.");
            }
            return url;
        }
    }
}
