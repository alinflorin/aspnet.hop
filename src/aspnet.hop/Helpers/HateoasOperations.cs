﻿namespace aspnet.hop.Helpers
{
    /// <summary>
    /// Notations for HATEOAS CRUD operations
    /// </summary>
    public enum HateoasOperations
    {
        Create = 0,
        List = 1,
        Show = 2,
        Update = 3,
        Delete = 4
    }
}
