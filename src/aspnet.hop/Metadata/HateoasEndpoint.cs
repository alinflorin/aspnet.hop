﻿using aspnet.hop.Routing;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Routing;
using Microsoft.Data.Entity;
using System;

namespace aspnet.hop.Metadata
{
    /// <summary>
    /// This contains info about the HATEOAS endpoints such as URL, object type, and others.
    /// Also, this class generates the routes for each endpoint
    /// </summary>
    /// <typeparam name="TContext">EF Context type</typeparam>
    /// <typeparam name="TEntity">Model type</typeparam>
    public class HateoasEndpoint<TContext, TEntity> : IHateoasEndpoint where TContext : DbContext where TEntity : class
    {
        // The second part of the full URL
        private readonly string _url;
        // The options passed as reference
        private HateoasOptions _options;
        // For general usage, so we won't call typeof(...)
        private readonly Type _entityType, _contextType;

        /// <summary>
        /// This will generate an endpoint with the default URL - lowercase class name
        /// </summary>
        public HateoasEndpoint():this(typeof(TEntity).Name.ToLower())
        {
        }

        /// <summary>
        /// Generates an endpoint with a custom URL
        /// </summary>
        /// <param name="url"></param>
        public HateoasEndpoint(string url)
        {
            _url = url;
            _entityType = typeof(TEntity);
            _contextType = typeof(TContext);
        }

        /// <summary>
        /// This builds the routes and maps them to the custom handler
        /// </summary>
        /// <returns></returns>
        public IRouter BuildRoutes()
        {
            var rb = new RouteBuilder
            {
                DefaultHandler = new HateoasRouteHandler<TContext, TEntity>(), //Mapping to handler
                ServiceProvider = _options.GetApp().ApplicationServices
            };
            rb.MapRoute( //Read
                name: "hateoas_" + GetUrl() + "_list",
                template: _options.GetBaseUrl() + "/" + GetUrl(),
                defaults: new { controller = "AbstractHateoas", action = "List" },
                constraints: new { httpMethod = new HttpMethodRouteConstraint("GET") }
            );
            rb.MapRoute( //Read
                name: "hateoas_" + GetUrl() + "_list_single",
                template: _options.GetBaseUrl() + "/" + GetUrl() + "/{modelId}",
                defaults: new { controller = "AbstractHateoas", action = "Show", modelId = 0 },
                constraints: new { httpMethod = new HttpMethodRouteConstraint("GET") }
            );
            rb.MapRoute( //Create
                name: "hateoas_" + GetUrl() + "_create",
                template: _options.GetBaseUrl() + "/" + GetUrl(),
                defaults: new { controller = "AbstractHateoas", action = "Create" },
                constraints: new { httpMethod = new HttpMethodRouteConstraint("POST") }
            );
            rb.MapRoute( //Update
                name: "hateoas_" + GetUrl() + "update",
                template: _options.GetBaseUrl() + "/" + GetUrl(),
                defaults: new { controller = "AbstractHateoas", action = "Update" },
                constraints: new { httpMethod = new HttpMethodRouteConstraint("PUT") }
            );
            rb.MapRoute( //Delete
                name: "hateoas_" + GetUrl() + "_delete",
                template: _options.GetBaseUrl() + "/" + GetUrl() + "/{modelId}",
                defaults: new { controller = "AbstractHateoas", action = "Delete", modelId = 0 },
                constraints: new { httpMethod = new HttpMethodRouteConstraint("DELETE") }
            );
            return rb.Build(); // Generates the routes as an IRoute object, so it will be added to the RouteCollection
        }

        /// <summary>
        /// Setter for the HateoasOptions
        /// </summary>
        /// <param name="opts"></param>
        public void SetOptions(HateoasOptions opts)
        {
            _options = opts;
        }

        /// <summary>
        /// Getter for Entity type
        /// </summary>
        /// <returns></returns>
        public Type GetEntityType()
        {
            return _entityType;
        }

        /// <summary>
        /// Getter for EF Context type
        /// </summary>
        /// <returns></returns>
        public Type GetContextType()
        {
            return _contextType;
        }

        /// <summary>
        /// Getter for the endpoint URL - trims slashes
        /// </summary>
        /// <returns></returns>
        public string GetUrl()
        {
            return _url.Trim('/').Trim('\\');
        }
    }
}
