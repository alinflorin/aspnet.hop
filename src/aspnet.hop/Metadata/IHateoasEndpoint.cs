﻿using Microsoft.AspNet.Routing;
using System;

namespace aspnet.hop.Metadata
{
    /// <summary>
    /// Interface for HateoasEndpoint.cs - so we can create a list of these disregarding the TEntity type
    /// </summary>
    public interface IHateoasEndpoint
    {
        void SetOptions(HateoasOptions opts);
        string GetUrl();
        IRouter BuildRoutes();
        Type GetEntityType();
        Type GetContextType();
    }
}
