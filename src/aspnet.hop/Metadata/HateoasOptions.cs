﻿using Microsoft.AspNet.Builder;
using System.Collections.Generic;
using Microsoft.Data.Entity;
using System.Linq;
using System;
using aspnet.hop.Metadata;
using System.Reflection;

namespace aspnet.hop.Metadata
{
    /// <summary>
    /// General HATEOAS options container - this is populated in Startup.cs in the lambda in UseHateoas(=>)
    /// </summary>
    public sealed class HateoasOptions
    {
        // The endpoints list is static so it can be used everywhere without passing reference
        public static List<IHateoasEndpoint> Endpoints { get; } = new List<IHateoasEndpoint>();
        // This is the same
        public static string BaseUrl { get; private set; } = "api";
        private readonly IApplicationBuilder _app;

        /// <summary>
        /// Builds the options
        /// </summary>
        /// <param name="app"></param>
        public HateoasOptions(IApplicationBuilder app)
        {
            _app = app;
        }

        /// <summary>
        /// Adds an endpoint, if valid, to the collection.
        /// </summary>
        /// <param name="endpoint"></param>
        /// <returns></returns>
        public HateoasOptions AddEndpoint(IHateoasEndpoint endpoint)
        {
            if (ValidatePk(endpoint))
            {
                endpoint.SetOptions(this);
                Endpoints.Add(endpoint);
            }
            return this;
        }

        /// <summary>
        /// Alternative way to add an endpoint with the default URL
        /// </summary>
        /// <typeparam name="TContext">The DbContext type</typeparam>
        /// <typeparam name="TEntity">The entity type</typeparam>
        /// <returns>this</returns>
        public HateoasOptions AddEndpoint<TContext, TEntity>() where TContext : DbContext where TEntity : class
        {
            return AddEndpoint(new HateoasEndpoint<TContext, TEntity>());
        }

        /// <summary>
        /// Alternative way to add an endpoint with a custom URL
        /// </summary>
        /// <typeparam name="TContext">The DbContext type</typeparam>
        /// <typeparam name="TEntity">The entity type</typeparam>
        /// <param name="url">The custom URL</param>
        /// <returns>this</returns>
        public HateoasOptions AddEndpoint<TContext, TEntity>(string url) where TContext : DbContext where TEntity : class
        {
            return AddEndpoint(new HateoasEndpoint<TContext, TEntity>(url));
        }

        /// <summary>
        /// Will create endpoints for all entities in a EF Context with the default URL - which were not added previously
        /// </summary>
        /// <typeparam name="TContext">The DbContext type</typeparam>
        /// <returns>this</returns>
        public HateoasOptions AddContext<TContext>() where TContext : DbContext
        {
            //Get the context instance from DI
            var context = _app.ApplicationServices.GetService(typeof(TContext)) as TContext;
            if (context == null)
            {
                throw new ArgumentException($"The context {typeof(TContext).Name} was not added in the application.");
            }
            //Get all entity types in the context
            var types = context.Model.GetEntityTypes().Select(t => t.ClrType).ToList();
            var existingTypes = GetEndPoints().Select(e => e.GetEntityType()).ToList();
            foreach (var type in types)
            {
                if (!existingTypes.Contains(type))
                { 
                    var endpointType = typeof(HateoasEndpoint<,>).MakeGenericType(typeof(TContext), type);
                    var endpoint = Activator.CreateInstance(endpointType) as IHateoasEndpoint;
                    AddEndpoint(endpoint); 
                }
            }
            return this;
        }


        /// <summary>
        /// Validates the Entity's PK - The PK should not be composite - just a single column
        /// </summary>
        /// <param name="endpoint">The HATEOAS endpoint</param>
        /// <returns>true if PK is single and valid</returns>
        private bool ValidatePk(IHateoasEndpoint endpoint)
        {
            //Get the Context from DI container
            var context = _app.ApplicationServices.GetService(endpoint.GetContextType()) as DbContext;
            var entityType = context?.Model.FindEntityType(endpoint.GetEntityType());
            var pk = entityType?.FindPrimaryKey();
            return pk?.Properties.Count == 1;
        }

        /// <summary>
        /// Sets the base URL for endpoints
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public HateoasOptions SetBaseUrl(string url)
        {
            BaseUrl = url;
            return this;
        }

        /// <summary>
        /// Getter for the base URL
        /// </summary>
        /// <returns></returns>
        public string GetBaseUrl()
        {
            return BaseUrl.Trim('/').Trim('\\');
        }

        /// <summary>
        /// Getter for IApplicationBuilder
        /// </summary>
        /// <returns></returns>
        public IApplicationBuilder GetApp()
        {
            return _app;
        }

        /// <summary>
        /// Gets the list of endpoints
        /// </summary>
        /// <returns></returns>
        public List<IHateoasEndpoint> GetEndPoints()
        {
            return Endpoints;
        }

    }
}

