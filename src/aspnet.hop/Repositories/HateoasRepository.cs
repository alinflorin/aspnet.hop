﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using aspnet.hop.Extensions.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.Data.Entity.Metadata;

namespace aspnet.hop.Repositories
{
    /// <summary>
    /// The database manager for EF models
    /// </summary>
    /// <typeparam name="TEntity">EF model type</typeparam>
    public class HateoasRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// The EF context instance - must be instantiated in the Controller
        /// </summary>
        private readonly DbContext _context;

        public HateoasRepository(DbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets an entity by its PK
        /// </summary>
        /// <param name="id">The PK value</param>
        /// <returns>An entity or null</returns>
        public async Task<TEntity> Get(object id)
        {
            return await _context.Set<TEntity>().Find(id);
        }

        /// <summary>
        /// Gets a list of entities taking account of the filtering parameters
        /// </summary>
        /// <param name="page">The page number</param>
        /// <param name="itemsPerPage">The number of items per page</param>
        /// <param name="orderBy">Column name to order by</param>
        /// <param name="orderDirection">asc or desc</param>
        /// <param name="parentKey">Parent's column name</param>
        /// <param name="parentKeyValue">Parent's column value</param>
        /// <returns></returns>
        public async Task<IEnumerable<TEntity>> GetAll(int? page, int? itemsPerPage, string orderBy, string orderDirection, string parentKey, object parentKeyValue)
        {
            var query = _context.Set<TEntity>().Where(c => true);

            //Parent filtering
            if (!string.IsNullOrEmpty(parentKey) && parentKeyValue != null)
            {
                //is parentKey valid?
                var entityType = _context.Model.FindEntityType(typeof(TEntity));
                if (entityType == null)
                {
                    return new List<TEntity>();
                }
                if (!entityType.GetNavigations()
                    .Any(n => !n.IsCollection() && n.ForeignKey != null
                    && n.ForeignKey.Properties.Count == 1 && n.ForeignKey.Properties[0].Name.Equals(parentKey)))
                {
                    return new List<TEntity>();
                }
                //filter them - dynamic Where clause
                var parameter = Expression.Parameter(typeof(TEntity), "x");
                query = query.Where((Expression<Func<TEntity, bool>>)
                    Expression.Lambda(
                        Expression.Equal(
                            Expression.Property(parameter, parentKey),
                            Expression.Constant(parentKeyValue)),
                        parameter));
            }

            //OrderBy
            if (!string.IsNullOrEmpty(orderBy))
            {
                if (orderDirection == null || orderDirection.ToLower().Equals("asc"))
                {
                    query = query.OrderBy(orderBy);

                }
                else
                {
                    query = query.OrderByDescending(orderBy);
                }
            }

            //Paging
            if (!page.HasValue || !(page >= 1)) return await query.ToListAsync();

            if (!itemsPerPage.HasValue)
            {
                itemsPerPage = 10;
            }
            query = query.Skip((page.Value - 1) * itemsPerPage.Value).Take(itemsPerPage.Value).Where(e => true);
            // FInish the query
            return await query.ToListAsync();
        }

        /// <summary>
        /// Adds an entity to the DB
        /// </summary>
        /// <param name="entity">The model</param>
        public void Add(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
        }

        /// <summary>
        /// Deletes an entity from the database
        /// </summary>
        /// <param name="entity">The model</param>
        public void Remove(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
        }

        /// <summary>
        /// Updates an entity in the DB
        /// </summary>
        /// <param name="entity">The model</param>
        public void Update(TEntity entity)
        {
            _context.Set<TEntity>().Update(entity);
        }

        /// <summary>
        /// Writes changes into the database
        /// </summary>
        /// <returns>Nothing</returns>
        public async Task SaveChanges()
        {
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets the PK value from an Entity Framework model
        /// </summary>
        /// <param name="model">The EF model</param>
        /// <returns>PK value as object</returns>
        public object GetPkValue(TEntity model)
        {
            // Try and find the PK column name
            var eType = _context.Model.FindEntityType(typeof(TEntity));
            if (eType?.FindPrimaryKey() == null || eType.FindPrimaryKey().Properties.Count != 1)
            {
                return null;
            }
            var pkColName = eType.FindPrimaryKey().Properties[0].Name;
            //Get the PK value using Reflection
            var pkProperty = typeof(TEntity).GetTypeInfo().DeclaredProperties.FirstOrDefault(p => p.Name.Equals(pkColName));
            return pkProperty?.GetValue(model);
        }

        /// <summary>
        /// Returns all navigation properties of model
        /// </summary>
        /// <returns>List of navigation properties generated by EF</returns>
        public IEnumerable<INavigation> GetNavigationProperties()
        {
            var eType = _context.Model.FindEntityType(typeof(TEntity));
            return eType?.GetNavigations().Where(n => n.ForeignKey != null && n.ForeignKey.Properties.Count == 1).ToList() ?? new List<INavigation>();
        }
    }
}
