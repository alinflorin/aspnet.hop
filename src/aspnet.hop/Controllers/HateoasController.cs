﻿using aspnet.hop.Exceptions;
using aspnet.hop.Metadata;
using aspnet.hop.Repositories;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using aspnet.hop.Extensions.Dictionary;

namespace aspnet.hop.Controllers
{
    /// <summary>
    /// This is the main controller, which is instantiated for any route and does all the hard work.
    /// </summary>
    /// <typeparam name="TContext">Entity Framework context type</typeparam>
    /// <typeparam name="TEntity">Model type</typeparam>
    public sealed class HateoasController<TContext, TEntity> : AbstractHateoasController where TEntity : class where TContext : DbContext
    {
        // An instance of the HateoasRepository
        private readonly HateoasRepository<TEntity> _repo;
        // Nicer error handling
        private readonly ExceptionHandler<TEntity> _exceptionHnd;
        private readonly Type _pkType;

        public HateoasController(TContext context)
        {
            if (context == null)
            {
                // We really need the EF context
                throw new NoEfContextInjectedExeption("No EF context was created and injected.");
            }
            _exceptionHnd = new ExceptionHandler<TEntity>();
            _repo = new HateoasRepository<TEntity>(context);
            var entityType = context.Model.FindEntityType(typeof(TEntity));
            _pkType = entityType == null ? typeof(int) : entityType.FindPrimaryKey().Properties[0].ClrType;
        }

        #region public
        /// <summary>
        /// Will return a (filtered) list of the entities
        /// </summary>
        /// <param name="page">The current page</param>
        /// <param name="itemsPerPage">Number of items per page</param>
        /// <param name="orderBy">Column name to order by</param>
        /// <param name="orderDirection">ASC or DESC</param>
        /// <param name="parentKey">Parent's FK name</param>
        /// <param name="parentKeyValue">Parent's FK value</param>
        /// <returns>JSON</returns>
        public override async Task<IActionResult> List(int? page, int? itemsPerPage, string orderBy, string orderDirection, string parentKey, string parentKeyValue)
        {
            object pk;
            if (!string.IsNullOrEmpty(parentKeyValue) && !string.IsNullOrEmpty(parentKey))
            {
                pk = ConvertStringToKeyType(parentKeyValue);
            } else
            {
                pk = parentKey;
            }
            var data = await _repo.GetAll(page, itemsPerPage, orderBy, orderDirection, parentKey, pk);
            var rez = data.Select(AddHypermediaUrls).ToList();
            return Ok(rez);
        }

        /// <summary>
        /// Will display a single entity
        /// </summary>
        /// <param name="modelId">PK value</param>
        /// <returns>JSON</returns>
        public override async Task<IActionResult> Show(string modelId)
        {
            try
            {
                var pk = ConvertStringToKeyType(modelId);
                var entity = await _repo.Get(pk);
                return entity == null ? _exceptionHnd.NotFound("The entity doesn't exist.") : Ok(AddHypermediaUrls(entity));
            } catch (Exception e)
            {
                return _exceptionHnd.Exception(e);
            }
        }

        /// <summary>
        /// Will create a new entity
        /// </summary>
        /// <param name="model">The entity model</param>
        /// <returns>JSON with the saved entity</returns>
        public override async Task<IActionResult> Create([FromBody]object model)
        {
            if (model == null)
            {
                return _exceptionHnd.Error("The object sent was null.");
            }
            try
            {
                var casted = JsonConvert.DeserializeObject<TEntity>(model.ToString());
                await TryUpdateModelAsync(casted);
                if (!ModelState.IsValid)
                {
                    return HttpBadRequest(ModelState);
                }
                _repo.Add(casted);
                await _repo.SaveChanges();
                return Ok(AddHypermediaUrls(casted));

            } catch (Exception e)
            {
                return _exceptionHnd.Exception(e);
            }
        }

        /// <summary>
        /// Will update an entity
        /// </summary>
        /// <param name="model">The entity model</param>
        /// <returns>JSON with the updated entity</returns>
        public override async Task<IActionResult> Update([FromBody]object model)
        {
            if (model == null)
            {
                return _exceptionHnd.Error("The object sent was null.");
            }
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }
            try
            {
                var casted = JsonConvert.DeserializeObject<TEntity>(model.ToString());
                await TryUpdateModelAsync(casted);
                if (!ModelState.IsValid)
                {
                    return HttpBadRequest(ModelState);
                }
                _repo.Update(casted);
                await _repo.SaveChanges();
                return Ok(AddHypermediaUrls(casted));
            }
            catch (Exception e)
            {
                return _exceptionHnd.Exception(e);
            }
        }

        /// <summary>
        /// Will delete an entity
        /// </summary>
        /// <param name="modelId">The PK's value</param>
        /// <returns>HTTP 200 or other</returns>
        public override async Task<IActionResult> Delete(string modelId)
        {
            try
            {
                var pk = ConvertStringToKeyType(modelId);
                var model = await _repo.Get(pk);
                if (model == null)
                {
                    return _exceptionHnd.Error("The entity was not found.");
                }
                _repo.Remove(model);
                await _repo.SaveChanges();
                return Ok("DELETED");
            } catch (Exception e)
            {
                return _exceptionHnd.Exception(e);
            }
        }
        #endregion

        #region private
        /// <summary>
        /// Converts the PK value from string (as it came in the URL) to it's actual type
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private object ConvertStringToKeyType(string s)
        {
            //Get the actual type
            var methodInfo = _pkType.GetTypeInfo().DeclaredMethods.FirstOrDefault(mi => mi.Name.Equals("Parse"));
            // Invoke Parse or return string
            return methodInfo == null ? s : methodInfo.Invoke(null, new object[] { s });
        }

        /// <summary>
        /// Build the full URL to the WEB API
        /// </summary>
        /// <returns></returns>
        private string AppUrlTrailingSlash()
        {
            var url = "http://";
            if (HttpContext.Request.IsHttps)
            {
                url = "https://";
            }
            url += HttpContext.Request.Host;
            url += "/";
            return url;
        }

        /// <summary>
        /// The main library logic "engine" - this appends all the HATEOAS URLs in place.
        /// </summary>
        /// <param name="model">The EF entity</param>
        /// <returns>A Dictionary with all the properties, links included.</returns>
        private IDictionary<string, object> AddHypermediaUrls(TEntity model)
        {
            // Convert the model to a dictionary
            var dict = model.AsDictionary();
            var pkValue = _repo.GetPkValue(model);
            if (pkValue == null) return dict;
            //find the other navigation properties
            var navis = _repo.GetNavigationProperties();
            foreach (var navi in navis)
            {
                var linkType = navi.IsCollection() ? navi.ForeignKey.DeclaringEntityType.ClrType : navi.ForeignKey.PrincipalEntityType.ClrType;
                var options = HateoasOptions.Endpoints.FirstOrDefault(e => e.GetEntityType() == linkType);
                if (options == null)
                {
                    continue;
                }
                if (!navi.IsCollection())
                {
                    //go to parent
                    var linkProperty = typeof(TEntity).GetTypeInfo().DeclaredProperties.FirstOrDefault(p => p.Name.Equals(navi.Name));
                    var parentKeyValueProp = typeof(TEntity).GetTypeInfo().DeclaredProperties.FirstOrDefault(p => p.Name.Equals(navi.ForeignKey.Properties[0].Name));
                    if (linkProperty == null || parentKeyValueProp == null) continue;
                    var parentKeyValue = parentKeyValueProp.GetValue(model);
                    if (parentKeyValue == null) continue;
                    var url = AppUrlTrailingSlash() + HateoasOptions.BaseUrl + "/" + options.GetUrl() + "/" + parentKeyValue;
                    dict[navi.Name] = url;
                }
                else
                {
                    //go to kids
                    var parentKeyName = navi.ForeignKey.Properties[0].Name;
                    var url =
                        $"{AppUrlTrailingSlash()}{HateoasOptions.BaseUrl}/{options.GetUrl()}?parentKey={parentKeyName}&parentKeyValue={pkValue}";
                    dict[navi.Name] = url;
                }
            }
            return dict;
        }

        #endregion

    }
}
