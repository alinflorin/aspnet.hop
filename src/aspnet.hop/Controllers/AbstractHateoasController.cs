﻿using Microsoft.AspNet.Mvc;
using System;
using System.Threading.Tasks;

namespace aspnet.hop.Controllers
{
    /// <summary>
    /// Exact copy of HateoasController. This is scanned by MVC and makes controllers of this type available for routing.
    /// We just change the type from AbstractHateoasController to HateoasController(TContext,TEntity). MVC doesn't mind :D
    /// </summary>
    public class AbstractHateoasController : Controller
    {
        /// <summary>
        /// Will return a (filtered) list of the entities - FAKE
        /// </summary>
        /// <param name="page">The current page</param>
        /// <param name="itemsPerPage">Number of items per page</param>
        /// <param name="orderBy">Column name to order by</param>
        /// <param name="orderDirection">ASC or DESC</param>
        /// <param name="parentKey">Parent's FK name</param>
        /// <param name="parentKeyValue">Parent's FK value</param>
        /// <returns>JSON</returns>
        public virtual Task<IActionResult> List(int? page, int? itemsPerPage, string orderBy, string orderDirection, string parentKey, string parentKeyValue)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Will display a single entity - FAKE
        /// </summary>
        /// <param name="modelId">PK value</param>
        /// <returns>JSON</returns>
        public virtual Task<IActionResult> Show(string modelId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Will create a new entity - FAKE
        /// </summary>
        /// <param name="model">The entity model</param>
        /// <returns>JSON with the saved entity</returns>
        public virtual Task<IActionResult> Create([FromBody]object model)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Will update an entity - FAKE
        /// </summary>
        /// <param name="model">The entity model</param>
        /// <returns>JSON with the updated entity</returns>
        public virtual Task<IActionResult> Update([FromBody]object model)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Will delete an entity - FAKE
        /// </summary>
        /// <param name="modelId">The PK's value</param>
        /// <returns>HTTP 200 or other</returns>
        public virtual Task<IActionResult> Delete(string modelId)
        {
            throw new NotImplementedException();
        }
    }
}
